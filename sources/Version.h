#ifndef VERSION_H
#define VERSION_H

// versioning following specifications on https://semver.org/

int version_major = 0;
int version_minor = 5;
int version_patch = 1;

#endif // VERSION_H
