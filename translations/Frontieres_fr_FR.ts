<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../sources/interface/AboutDialog.ui" line="14"/>
        <source>About Frontières</source>
        <translation>À propos de Frontières</translation>
    </message>
    <message>
        <location filename="../sources/interface/AboutDialog.ui" line="20"/>
        <source>&lt;h1&gt;Frontières&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Frontières&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../sources/interface/AboutDialog.ui" line="30"/>
        <source>&lt;h3&gt;An interactive granular sampler&lt;/h3&gt;</source>
        <translation>&lt;h3&gt;Un échantillonneur granulaire interactif&lt;/h3&gt;</translation>
    </message>
    <message>
        <location filename="../sources/interface/AboutDialog.ui" line="40"/>
        <source>&lt;a href=&quot;https://github.com/linuxmao-org/Frontieres&quot;&gt;https://github.com/linuxmao-org/Frontieres&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;https://github.com/linuxmao-org/Frontieres&quot;&gt;https://github.com/linuxmao-org/Frontieres&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../sources/interface/AboutDialog.ui" line="66"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Authors:&lt;/span&gt; Jean Pierre Cimalando, Olivier Flatrès, Christopher Carlson (&lt;a href=&quot;http://www.borderlands-granular.com/app/&quot;&gt;Borderlands&lt;/a&gt;)&lt;br/&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Contributors:&lt;/span&gt; Olivier Humbert&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Auteurs :&lt;/span&gt; Jean Pierre Cimalando, Olivier Flatrès, Christopher Carlson (&lt;a href=&quot;http://www.borderlands-granular.com/app/&quot;&gt;Borderlands&lt;/a&gt;)&lt;br/&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Contributeurs :&lt;/span&gt; Olivier Humbert&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../sources/interface/AboutDialog.ui" line="92"/>
        <source>This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.</source>
        <translation>Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier sous les termes de la licence publique générale GNU telle que publiée par la Free Software Foundation, version 3.</translation>
    </message>
</context>
<context>
    <name>AdsrDialog</name>
    <message>
        <location filename="../sources/interface/AdsrDialog.ui" line="14"/>
        <source>Cloud envelope</source>
        <translation>Enveloppe de nuage</translation>
    </message>
    <message>
        <location filename="../sources/interface/AdsrDialog.ui" line="41"/>
        <source>Attack/Slope</source>
        <translation>Attaque/pente</translation>
    </message>
    <message>
        <location filename="../sources/interface/AdsrDialog.ui" line="60"/>
        <location filename="../sources/interface/AdsrDialog.ui" line="218"/>
        <location filename="../sources/interface/AdsrDialog.ui" line="300"/>
        <location filename="../sources/interface/AdsrDialog.ui" line="420"/>
        <source>Length (sec)</source>
        <translation>Longueur (sec)</translation>
    </message>
    <message>
        <location filename="../sources/interface/AdsrDialog.ui" line="98"/>
        <location filename="../sources/interface/AdsrDialog.ui" line="338"/>
        <source>Height</source>
        <translation>Hauteur</translation>
    </message>
    <message>
        <location filename="../sources/interface/AdsrDialog.ui" line="136"/>
        <location filename="../sources/interface/AdsrDialog.ui" line="256"/>
        <location filename="../sources/interface/AdsrDialog.ui" line="376"/>
        <location filename="../sources/interface/AdsrDialog.ui" line="496"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../sources/interface/AdsrDialog.ui" line="161"/>
        <source>Release</source>
        <translation>Relâche</translation>
    </message>
    <message>
        <location filename="../sources/interface/AdsrDialog.ui" line="180"/>
        <location filename="../sources/interface/AdsrDialog.ui" line="458"/>
        <source>Height (sustain)</source>
        <translation>Hauteur (soutien)</translation>
    </message>
    <message>
        <location filename="../sources/interface/AdsrDialog.ui" line="281"/>
        <source>Delay/Attack</source>
        <translation>Délai/attaque</translation>
    </message>
    <message>
        <location filename="../sources/interface/AdsrDialog.ui" line="401"/>
        <source>Decay/Attack2</source>
        <translation>Déclin/attaque2</translation>
    </message>
</context>
<context>
    <name>BankDialog</name>
    <message>
        <location filename="../sources/interface/BankDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <location filename="../sources/interface/BankDialog.cpp" line="66"/>
        <source>Combination</source>
        <translation>Combinaison</translation>
    </message>
</context>
<context>
    <name>CloudDialog</name>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <source>Volume (b)</source>
        <translation>Volume (b)</translation>
    </message>
    <message>
        <source>Envelope (e)</source>
        <translation>Enveloppe (e)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="339"/>
        <source>Name : </source>
        <translation>Nom : </translation>
    </message>
    <message>
        <source>Grains (v)</source>
        <translation>Grains (v)</translation>
    </message>
    <message>
        <source>LFO Freq (l)</source>
        <translation>Fréq LFO (1)</translation>
    </message>
    <message>
        <source>Pitch (z)</source>
        <translation>Tonalité (z)</translation>
    </message>
    <message>
        <source>X Extent (x)</source>
        <translation>X Extension (x)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="1307"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <source>Overlap (s)</source>
        <translation>Chevauchement (s)</translation>
    </message>
    <message>
        <source>LFO Amp (k)</source>
        <translation>Amp LFO (k)</translation>
    </message>
    <message>
        <source>Duration (d)</source>
        <translation>Durée (d)</translation>
    </message>
    <message>
        <source>Y Extent (y)</source>
        <translation>Extension Y (y)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="2398"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="2860"/>
        <source>Progress</source>
        <translation>Progression</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="3288"/>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="3526"/>
        <source>Radius Int</source>
        <translation>Rayon interne</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="3719"/>
        <source>Expansion</source>
        <translation>Extension</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4528"/>
        <source>Restart</source>
        <translation>Redémarre</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4705"/>
        <source>Direction (F)</source>
        <translation>Direction (F)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4797"/>
        <source>Trajectory (I)</source>
        <translation>Trajectoire (I)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4826"/>
        <source>&amp;Circular</source>
        <translation>&amp;Circulaire</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4836"/>
        <source>&amp;Hypotrochoid</source>
        <translation>&amp;Hypotrochoïde</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4999"/>
        <source>Window (W)</source>
        <translation>Fenêtre (W)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="5028"/>
        <source>&amp;Expdec</source>
        <translation>&amp;Expdec</translation>
    </message>
    <message>
        <source>Strech</source>
        <translation>Etirement</translation>
    </message>
    <message>
        <source>Trajectory (i)</source>
        <translation>Trajectoire (I)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4806"/>
        <source>&amp;Static</source>
        <translation>&amp;Statique</translation>
    </message>
    <message>
        <source>&amp;Boucing</source>
        <translation>&amp;Rebond</translation>
    </message>
    <message>
        <source>Ci&amp;rcular</source>
        <translation>Ci&amp;rculaire</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="3074"/>
        <source>Radius</source>
        <translation>Rayon</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="2651"/>
        <source>Speed</source>
        <translation>Vitesse</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="132"/>
        <source>Volume (B)</source>
        <translation>Volume (B)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="258"/>
        <source>Envelope (E)</source>
        <translation>Enveloppe (E)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="457"/>
        <source>Grains (V)</source>
        <translation>Grains (V)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="662"/>
        <source>LFO Freq (L)</source>
        <translation>Fréq LFO (L)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="876"/>
        <source>Pitch (Z)</source>
        <translation>Hauteur (Z)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="1093"/>
        <source>X Extent (X)</source>
        <translation>X Extension (X)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="1557"/>
        <source>Overlap (S)</source>
        <translation>Chevauchement (S)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="1759"/>
        <source>LFO Amp (K)</source>
        <translation>Amp LFO (K)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="1970"/>
        <source>Duration (D)</source>
        <translation>Durée (D)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="2184"/>
        <source>Y Extent (Y)</source>
        <translation>Extension Y (Y)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4147"/>
        <source>Balance (T)</source>
        <translation>Balance (T)</translation>
    </message>
    <message>
        <source>&amp;Unity</source>
        <translation>&amp;Mono</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4163"/>
        <source>&amp;Stereo</source>
        <translation>&amp;Stéréo</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4500"/>
        <source>Id : </source>
        <translation>Id : </translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4514"/>
        <source>Active</source>
        <translation>Actif</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4521"/>
        <source>Locked</source>
        <translation>Verrouillé</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4606"/>
        <source>Midi Note</source>
        <translation>Note MIDI</translation>
    </message>
    <message>
        <source>Direction (f)</source>
        <translation>Direction (f)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4717"/>
        <source>&amp;Forward</source>
        <translation>&amp;En avant</translation>
    </message>
    <message>
        <source>Bac&amp;kward</source>
        <translation>En arrière (&amp;k)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4730"/>
        <source>&amp;Backward</source>
        <translation>&amp;En arrière</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4737"/>
        <location filename="../sources/interface/CloudDialog.ui" line="5049"/>
        <source>&amp;Random</source>
        <translation>&amp;Aléatoire</translation>
    </message>
    <message>
        <source>Balance (t)</source>
        <translation>Balance (t)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="3933"/>
        <source>Stretch</source>
        <translation>Etirement</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4156"/>
        <source>Unit&amp;y</source>
        <translation>&amp;Unité</translation>
    </message>
    <message>
        <source>S&amp;tereo</source>
        <translation>S&amp;téréo</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4173"/>
        <source>&amp;Around</source>
        <translation>&amp;Autour</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4251"/>
        <source>First output</source>
        <translation>Première sortie</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4368"/>
        <source>Last output</source>
        <translation>Dernière sortie</translation>
    </message>
    <message>
        <source>Rando&amp;m</source>
        <translation>&amp;Aléatoire</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4819"/>
        <source>&amp;Bouncing</source>
        <translation>&amp;Rebond</translation>
    </message>
    <message>
        <source>H&amp;ypotrochoid</source>
        <translation>H&amp;ypotrochoïde</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="4843"/>
        <source>&amp;Recorded</source>
        <translation>En&amp;registrée</translation>
    </message>
    <message>
        <source>Window (w)</source>
        <translation>Fenêtre (w)</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="5008"/>
        <source>&amp;Hanning</source>
        <translation>&amp;Hanning</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="5015"/>
        <source>&amp;Triangle</source>
        <translation>&amp;Triangle</translation>
    </message>
    <message>
        <source>E&amp;xpdec</source>
        <translation>E&amp;xpdec</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="5035"/>
        <source>&amp;Rexpdec</source>
        <translation>&amp;Rexpdec</translation>
    </message>
    <message>
        <location filename="../sources/interface/CloudDialog.ui" line="5042"/>
        <source>&amp;Sinc</source>
        <translation>&amp;Syncronisé</translation>
    </message>
</context>
<context>
    <name>CombiDialog</name>
    <message>
        <location filename="../sources/interface/CombiDialog.ui" line="17"/>
        <source>Combination</source>
        <translation>Combinaison</translation>
    </message>
    <message>
        <location filename="../sources/interface/CombiDialog.ui" line="53"/>
        <source>Name :</source>
        <translation>Nom : </translation>
    </message>
    <message>
        <location filename="../sources/interface/CombiDialog.ui" line="177"/>
        <source>Range begin</source>
        <translation>Début de la plage</translation>
    </message>
    <message>
        <location filename="../sources/interface/CombiDialog.ui" line="233"/>
        <source>Range end</source>
        <translation>Fin de la plage</translation>
    </message>
    <message>
        <location filename="../sources/interface/CombiDialog.ui" line="259"/>
        <source>&amp;Add</source>
        <translation>&amp;Ajouter</translation>
    </message>
    <message>
        <location filename="../sources/interface/CombiDialog.ui" line="276"/>
        <source>&amp;Remove</source>
        <translation>Enleve&amp;r</translation>
    </message>
    <message>
        <location filename="../sources/interface/CombiDialog.ui" line="343"/>
        <source>Velocity Min</source>
        <translation>Vélocité min</translation>
    </message>
    <message>
        <location filename="../sources/interface/CombiDialog.ui" line="402"/>
        <source>Velocity Max</source>
        <translation>Vélocité max</translation>
    </message>
</context>
<context>
    <name>InstrumentDialog</name>
    <message>
        <location filename="../sources/interface/InstrumDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <location filename="../sources/interface/InstrumDialog.cpp" line="49"/>
        <source>Combinations</source>
        <translation>Combinaison</translation>
    </message>
</context>
<context>
    <name>MyGLApplication</name>
    <message>
        <location filename="../sources/interface/MyGLApplication.cpp" line="218"/>
        <source>Load sample</source>
        <translation>Charger l&apos;échantillon</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLApplication.cpp" line="219"/>
        <source>Sample files (*.*)</source>
        <translation>Fichiers d&apos;échantillon (*.*)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLApplication.cpp" line="254"/>
        <source>Cloud parameters</source>
        <translation>Paramètres du nuage</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLApplication.cpp" line="279"/>
        <source>Trigger parameters</source>
        <translation>Paramètres du déclencheur</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLApplication.cpp" line="357"/>
        <source>Combinations</source>
        <translation>Combinaisons</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLApplication.cpp" line="366"/>
        <source>Midi instrument</source>
        <translation>Instrument MIDI</translation>
    </message>
</context>
<context>
    <name>MyGLWindow</name>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="14"/>
        <source>Frontières</source>
        <translation>Frontières</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="46"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="57"/>
        <source>&amp;Edit</source>
        <translation>Édit&amp;ion</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="65"/>
        <source>&amp;OSC</source>
        <translation>&amp;OSC</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="71"/>
        <source>&amp;Midi</source>
        <translation>&amp;Midi</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="78"/>
        <source>&amp;Commands</source>
        <translation>&amp;Commandes</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="103"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="118"/>
        <source>&amp;Load scene</source>
        <translation>Charger une scène (&amp;L)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="121"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="126"/>
        <source>&amp;Save scene</source>
        <translation>&amp;Sauvegarder la scène</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="129"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="134"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="139"/>
        <source>&amp;Add samples...</source>
        <translation>&amp;Ajouter des échantillons...</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="144"/>
        <source>Load &amp;clouds defaults</source>
        <translation>&amp;Charger les nuages par défault</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="149"/>
        <source>&amp;Start controller</source>
        <translation>Démarrer le contrôleur (&amp;S)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="154"/>
        <source>&amp;Options</source>
        <translation>&amp;Options</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="159"/>
        <source>&amp;Control</source>
        <translation>&amp;Contrôle</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="164"/>
        <source>&amp;Combinations</source>
        <translation>&amp;Combinaisons</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="169"/>
        <source>&amp;Instrument</source>
        <translation>&amp;Instrument</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="174"/>
        <source>C&amp;ombi</source>
        <translation>C&amp;ombi</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="179"/>
        <source>Full screen (&amp;O)</source>
        <translation>Plein écran (&amp;O)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="184"/>
        <source>Spatial mode (&amp;T)</source>
        <translation>Balance panoramique (&amp;T)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="189"/>
        <source>Overlap (&amp;S)</source>
        <translation>Chevauchement (&amp;S)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="194"/>
        <source>X Exp (&amp;X)</source>
        <translation>X Extension (&amp;X)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="199"/>
        <source>Y Exp (&amp;Y)</source>
        <translation>Y Extension (&amp;Y)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="204"/>
        <source>XY Exp (&amp;R)</source>
        <translation>XY Extension (&amp;R)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="224"/>
        <source>Grains (&amp;V)</source>
        <translation>Grains (&amp;V)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="227"/>
        <location filename="../sources/interface/MyGLWindow.ui" line="230"/>
        <source>Grains (V)</source>
        <translation>Grains (V)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="290"/>
        <source>Trajectory (&amp;I)</source>
        <translation>Trajectoire (&amp;I)</translation>
    </message>
    <message>
        <source>X motion (&amp;X)</source>
        <translation>Mouvement X (&amp;X)</translation>
    </message>
    <message>
        <source>Y motion (&amp;Y)</source>
        <translation>Mouvement Y (&amp;Y)</translation>
    </message>
    <message>
        <source>XY motion (&amp;R)</source>
        <translation>Mouvement XY (&amp;R)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="209"/>
        <source>Window (&amp;W)</source>
        <translation>Fenêtre (&amp;W)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="214"/>
        <source>Volume (&amp;B)</source>
        <translation>Volume (&amp;B)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="219"/>
        <source>Cloud (&amp;G)</source>
        <translation>Nuage (&amp;G)</translation>
    </message>
    <message>
        <source>Grain (&amp;V)</source>
        <translation>Grain (&amp;V)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="235"/>
        <source>Duration (&amp;D)</source>
        <translation>Durée (&amp;D)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="240"/>
        <source>LFO frequency (&amp;L)</source>
        <translation>Fréquence LFO (&amp;L)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="245"/>
        <source>LFO amount (&amp;K)</source>
        <translation>Quantité LFO (&amp;K)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="250"/>
        <source>Pitch (&amp;Z)</source>
        <translation>Tonalité (&amp;Z)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="255"/>
        <source>Active (&amp;A)</source>
        <translation>Actif (&amp;A)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="260"/>
        <source>Edit cloud (&amp;P)</source>
        <translation>Éditer le nuage (&amp;P)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="265"/>
        <source>Edit envelope (&amp;E)</source>
        <translation>Éditer l&apos;enveloppe (&amp;E)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="270"/>
        <source>About &amp;Frontières</source>
        <translation>À propos de &amp;Frontières</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="275"/>
        <source>User &amp;manual</source>
        <translation>&amp;Manuel utilisateur</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="280"/>
        <source>Sample names (&amp;N)</source>
        <translation>Noms d&apos;échantillons (&amp;N)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.ui" line="285"/>
        <source>Direction (&amp;F)</source>
        <translation>Direction (&amp;F)</translation>
    </message>
    <message>
        <source>Trajectory (I)</source>
        <translation>Trajectoire (i)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.cpp" line="171"/>
        <source>User manual</source>
        <translation>Manuel utilisateur</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.cpp" line="222"/>
        <source>DSP%</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <source>OSC</source>
        <translation>OSC</translation>
    </message>
    <message>
        <location filename="../sources/interface/OptionsDialog.ui" line="14"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../sources/interface/OptionsDialog.ui" line="54"/>
        <source>&amp;Start controller</source>
        <translation>Démarrer le contrôleur (&amp;S)</translation>
    </message>
    <message>
        <location filename="../sources/interface/OptionsDialog.ui" line="88"/>
        <source>Restart OSC</source>
        <translation>Redémarrer OSC</translation>
    </message>
    <message>
        <location filename="../sources/interface/OptionsDialog.ui" line="122"/>
        <source>OSC Port : </source>
        <translation>Port OSC :</translation>
    </message>
    <message>
        <location filename="../sources/interface/OptionsDialog.ui" line="129"/>
        <source>99999</source>
        <translation>99999</translation>
    </message>
    <message>
        <location filename="../sources/interface/OptionsDialog.cpp" line="38"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../sources/interface/OptionsDialog.cpp" line="39"/>
        <source>Cannot lanch the program: %1</source>
        <translation>Impossible de lancer le programme : %1</translation>
    </message>
    <message>
        <location filename="../sources/interface/OptionsDialog.cpp" line="40"/>
        <source>Please ensure the software is installed and the path is correct.</source>
        <translation>Veuillez vous assurer que le logiciel est installé et que le chemin est correct.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../sources/Frontieres.cpp" line="445"/>
        <source>CLICK TO START</source>
        <translation>CLIQUER POUR DÉMARRER</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="453"/>
        <source>ESCAPE TO QUIT</source>
        <translation>ÉCHAP POUR QUITTER</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="482"/>
        <source>Grains: </source>
        <translation>Grains : </translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="489"/>
        <source>Duration: </source>
        <translation>Durée : </translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="504"/>
        <source>Window: HANNING</source>
        <translation>Fenêtre : HANNING</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="507"/>
        <source>Window: TRIANGLE</source>
        <translation>Fenêtre : TRIANGLE</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="510"/>
        <source>Window: REXPDEC</source>
        <translation>Fenêtre : REXPDEC</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="513"/>
        <source>Window: EXPDEC</source>
        <translation>Fenêtre : EXPDEC</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="516"/>
        <source>Window: SINC</source>
        <translation>Fenêtre : SINC</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="519"/>
        <source>Window: RANDOM_WIN</source>
        <translation>Fenêtre : RANDOM_WIN</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="530"/>
        <source>X: </source>
        <translation>&gt;X : </translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="537"/>
        <source>Y: </source>
        <translation>Y : </translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="544"/>
        <source>X,Y: </source>
        <translation>X, Y : </translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="556"/>
        <source>Direction: FORWARD</source>
        <translation>Direction : EN AVANT</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="559"/>
        <source>Direction: BACKWARD</source>
        <translation>Direction : EN ARRIÈRE</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="562"/>
        <source>Direction: RANDOM</source>
        <translation>Direction : HASARD</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="575"/>
        <source>Spatial Mode: UNITY</source>
        <translation>Balance panoramique : UNITÉ</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="578"/>
        <source>Spatial Mode: STEREO</source>
        <translation>Balance panoramique : STÉRÉO</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="581"/>
        <source>Spatial Mode: AROUND</source>
        <translation>Balance panoramique : AUTOUR</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="591"/>
        <source>Volume (dB): </source>
        <translation>Volume (dB) : </translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="603"/>
        <source>Overlap: </source>
        <translation>Chevauchement : </translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="616"/>
        <source>Pitch: </source>
        <translation>Tonalité : </translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="630"/>
        <source>Pitch LFO Freq: </source>
        <translation>Fréquence LFO de la tonalité : </translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="643"/>
        <source>Pitch LFO Amount: </source>
        <translation>Quantité LFO de la tonalité : </translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="656"/>
        <source>Cloud ID/Num: </source>
        <translation>ID/num nuage : </translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="666"/>
        <source>Trajectory: STATIC</source>
        <translation>Trajectoire : STATIQUE</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="669"/>
        <source>Trajectory: BOUNCING</source>
        <translation>Trajectoire : REBOND</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="672"/>
        <source>Trajectory: CIRCULAR</source>
        <translation>Trajectoire : CIRCULAIRE</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="675"/>
        <source>Trajectory: HYPOTROCHOID</source>
        <translation>Trajectoire: HYPOTROCHOIDE</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="678"/>
        <source>Trajectory: RECORDED</source>
        <translation>Trajectoire: ENREGISTRÉE</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="700"/>
        <source>Sample: </source>
        <translation>Échantillon : </translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="922"/>
        <source>Interactive granular sampler</source>
        <translation>Échantillonneur granulaire interactif</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="928"/>
        <source>Set the number of output channels.</source>
        <translation>Paramétrer le nombre de canaux de sortie.</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="929"/>
        <source>channel-count</source>
        <translation>nombre-canaux</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="934"/>
        <source>Automatically connect audio streams to the output device.</source>
        <translation>Connecte automatiquement les flux audio au périphérique de sortie.</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="939"/>
        <source>Set the refresh rate in frames per second.</source>
        <translation>Paramétrer le taux de rafraîchissement en trames par seconde.</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="940"/>
        <source>refresh-rate</source>
        <translation>taux de rafraîchissement</translation>
    </message>
    <message>
        <location filename="../sources/Frontieres.cpp" line="952"/>
        <source>Invalid FPS value.</source>
        <translation>Valeur de trames par seconde invalide.</translation>
    </message>
    <message>
        <location filename="../sources/model/Scene.cpp" line="71"/>
        <source>Scene files (*%1)</source>
        <translation>Fichiers de scène (*%1)</translation>
    </message>
    <message>
        <location filename="../sources/model/Scene.cpp" line="77"/>
        <source>Save scene</source>
        <translation>Sauvegarder la scène</translation>
    </message>
    <message>
        <location filename="../sources/model/Scene.cpp" line="81"/>
        <source>Load scene</source>
        <translation>Charger une scène</translation>
    </message>
    <message>
        <location filename="../sources/model/Scene.cpp" line="113"/>
        <source>Cloud files (*%1)</source>
        <translation>Fichiers de nuage (*%1)</translation>
    </message>
    <message>
        <location filename="../sources/model/Scene.cpp" line="119"/>
        <source>Save cloud</source>
        <translation>Sauvegarder le nuage</translation>
    </message>
    <message>
        <location filename="../sources/model/Scene.cpp" line="123"/>
        <source>Load cloud</source>
        <translation>Charger le nuage</translation>
    </message>
    <message>
        <location filename="../sources/model/Scene.cpp" line="142"/>
        <source>Trajectory files (*%1)</source>
        <translation>Fichiers de trajectoires (*%1)</translation>
    </message>
    <message>
        <location filename="../sources/model/Scene.cpp" line="1682"/>
        <source>Load samples</source>
        <translation>Charger les échantillons</translation>
    </message>
    <message>
        <location filename="../sources/model/Scene.cpp" line="1683"/>
        <source>Could not find the sample file &quot;%1&quot;.</source>
        <translation>Impossible de trouver le fichier d&apos;échantillon &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../sources/model/Scene.cpp" line="1685"/>
        <source>Add a different sample directory</source>
        <translation>Ajouter un répertoire d&apos;échantillon différent</translation>
    </message>
    <message>
        <location filename="../sources/model/Scene.cpp" line="1686"/>
        <source>Discard missing samples</source>
        <translation>Abandonner les échantillons manquants</translation>
    </message>
    <message>
        <location filename="../sources/model/Scene.cpp" line="1700"/>
        <source>Add sample directory</source>
        <translation>Ajouter un répertoire d&apos;échantillon</translation>
    </message>
    <message>
        <location filename="../sources/interface/BankDialog.cpp" line="94"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../sources/interface/BankDialog.cpp" line="95"/>
        <source>Paste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <location filename="../sources/interface/BankDialog.cpp" line="96"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Cloud parameters</source>
        <translation>Paramètres du nuage</translation>
    </message>
    <message>
        <source>Create new cloud</source>
        <translation>Nouveau nuage</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.cpp" line="1073"/>
        <source>Cloud parameters (P)</source>
        <translation>Paramètres du nuage (P)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.cpp" line="1044"/>
        <location filename="../sources/interface/MyGLWindow.cpp" line="1079"/>
        <location filename="../sources/interface/MyGLWindow.cpp" line="1108"/>
        <source>Create new cloud (G)</source>
        <translation>Nouveau nuage (G)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.cpp" line="1038"/>
        <source>Trigger parameters (P)</source>
        <translation>Paramètres du déclencheur (P)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.cpp" line="1050"/>
        <location filename="../sources/interface/MyGLWindow.cpp" line="1085"/>
        <location filename="../sources/interface/MyGLWindow.cpp" line="1114"/>
        <source>Create new trigger (H)</source>
        <translation>Nouveau déclencheur (H)</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.cpp" line="1056"/>
        <location filename="../sources/interface/MyGLWindow.cpp" line="1091"/>
        <source>Record trajectory</source>
        <translation>Enregistrer la trajectoire</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.cpp" line="1059"/>
        <location filename="../sources/interface/MyGLWindow.cpp" line="1094"/>
        <location filename="../sources/model/Scene.cpp" line="152"/>
        <source>Load trajectory</source>
        <translation>Charger une trajectoire</translation>
    </message>
    <message>
        <location filename="../sources/interface/MyGLWindow.cpp" line="1063"/>
        <location filename="../sources/interface/MyGLWindow.cpp" line="1098"/>
        <location filename="../sources/model/Scene.cpp" line="148"/>
        <source>Save trajectory</source>
        <translation>Sauvegarder la trajectoire</translation>
    </message>
</context>
<context>
    <name>StartDialog</name>
    <message>
        <location filename="../sources/interface/StartDialog.ui" line="14"/>
        <source>Get started</source>
        <translation>Commencez à travailler</translation>
    </message>
    <message>
        <location filename="../sources/interface/StartDialog.ui" line="35"/>
        <source>New scene</source>
        <translation>Nouvelle scène</translation>
    </message>
    <message>
        <location filename="../sources/interface/StartDialog.ui" line="38"/>
        <source>Create a new workspace using a default sound set.</source>
        <translation>Créer un nouvel espace de travail utilisant un ensemble de sons par défaut.</translation>
    </message>
    <message>
        <location filename="../sources/interface/StartDialog.ui" line="58"/>
        <source>Load scene</source>
        <translation>Charger la scène</translation>
    </message>
    <message>
        <location filename="../sources/interface/StartDialog.ui" line="61"/>
        <source>Resume work with an existing scene file.</source>
        <translation>Reprendre le travail avec un fichier de scène existant.</translation>
    </message>
</context>
<context>
    <name>TriggerDialog</name>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="163"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="383"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="597"/>
        <source>Extent (XY)</source>
        <translation>Extension (xy)</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="781"/>
        <source>Action In</source>
        <translation>Extension à l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="797"/>
        <location filename="../sources/interface/TriggerDialog.ui" line="899"/>
        <source>&amp;On</source>
        <translation>&amp;On</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="807"/>
        <location filename="../sources/interface/TriggerDialog.ui" line="909"/>
        <source>&amp;Off</source>
        <translation>&amp;Off</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="820"/>
        <location filename="../sources/interface/TriggerDialog.ui" line="922"/>
        <source>&amp;Commute</source>
        <translation>&amp;Commute</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="883"/>
        <source>Action Out</source>
        <translation>Action à la sortie</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="790"/>
        <location filename="../sources/interface/TriggerDialog.ui" line="892"/>
        <source>&amp;Nothing</source>
        <translation>&amp;Rien</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="1072"/>
        <source>Speed</source>
        <translation>Vitesse</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="1286"/>
        <source>Progress</source>
        <translation>Progression</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="1500"/>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="1714"/>
        <source>Radius</source>
        <translation>Rayon</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="1928"/>
        <source>Radius Int</source>
        <translation>Rayon interne</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="2142"/>
        <source>Stretch</source>
        <translation>Etirement</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="2359"/>
        <source>Expansion</source>
        <translation>Extension</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="2549"/>
        <source>Name : </source>
        <translation>Nom : </translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="2667"/>
        <source>Id : </source>
        <translation>Id : </translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="2681"/>
        <source>Active</source>
        <translation>Actif</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="2688"/>
        <source>Locked</source>
        <translation>Verrouillé</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="2695"/>
        <source>Restart</source>
        <translation>Redémarre</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="2743"/>
        <source>Trajectory (I)</source>
        <translation>Trajectoire (I)</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="2752"/>
        <source>&amp;Static </source>
        <translation>&amp;Statique</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="2771"/>
        <source>&amp;Bouncing</source>
        <translation>&amp;Rebond</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="2778"/>
        <source>&amp;Circular</source>
        <translation>&amp;Circulaire</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="2788"/>
        <source>&amp;Hypotrochoid</source>
        <translation>&amp;Hypotrochoïde</translation>
    </message>
    <message>
        <location filename="../sources/interface/TriggerDialog.ui" line="2795"/>
        <source>&amp;Recorded</source>
        <translation>&amp;Enregistrée</translation>
    </message>
</context>
</TS>
